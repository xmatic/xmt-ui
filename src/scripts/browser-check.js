;(function () {
  function get_browser() {
    var ua = navigator.userAgent;
    var tem;
    var M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return {name:'IE', version:(tem[1] || '')};
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\bOPR|Edge\/(\d+)/);
      if (tem != null) return {name:tem[0].replace('OPR', 'Opera'), version:tem[1]};
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem=ua.match(/version\/(\d+)/i)) != null) M.splice(1,1,tem[1]);

    return {name: M[0], version: M[1]};
  }

  function setcss(el, styles) {
    for (var property in styles)
      el.style[property] = styles[property];
  }

  var browser = get_browser();

  if (browser.name === 'MSIE'
    || (browser.name === 'Chrome' && browser.version < 50)
    || (browser.name === 'Opera' && browser.version < 50)
    || (browser.name === 'Safari' && browser.version < 9)
    || (browser.name === 'Firefox' && browser.version < 52)) {

    window.onload = function() {
      var div = document.createElement('div');
      if (browser.name === 'MSIE') {
        div.innerText ='Вы используете устаревшую версию браузера, могут быть ошибки совместимости при просмотре графики.';
      } else {
        div.textContent='Вы используете устаревшую версию браузера, могут быть ошибки совместимости при просмотре графики.';
      }

      var style = {
        'display': 'block',
        'position': 'fixed',
        'top': '0px',
        'left': '0px',
        'z-index': '99999',
        'width': '100%',
        'color': '#856404',
        'background-color': '#fff3cd',
        'background': '#fff3cd',
        'border-color': '#ffeeba',
        'padding': '10px 18px 10px 18px',
        'border': '1px solid',
        'text-align': 'center'
      }
      setcss(div, style);

      var button = document.createElement('button');
      style = {
        'padding': '0',
        'cursor': 'pointer',
        'background': '0 0',
        'border': '0',
        '-webkit-appearance': 'none',
        'float': 'right',
        'font-size': '20px',
        'font-weight': '700',
        'line-height': '1',
        'color': '#000',
        'opacity': '0.5'
      }
      setcss(button, style);

      if (button.addEventListener) {
        button.addEventListener("click", function() { div.style.display = 'none';});
      }
      else {
        button.attachEvent("onclick", function() {div.style.display = 'none';});
      }

      var span = document.createElement('span');
      if (browser.name === 'MSIE') {
        span.innerText = '\u2715';
      } else {
        span.textContent = '\u2715';
      }

      button.appendChild(span);
      div.appendChild(button);
      document.body.insertBefore(div, document.body.firstChild);


      setInterval(function () {
        div.style.display = 'block';
      }, 1800000)
    };
  }

})();
