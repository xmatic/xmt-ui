import './vendor.ts';

import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {LayoutRoutingModule} from './layouts/layout-routing.module';
// layout
import {MainComponent} from './layouts/main/main.component';
import {HeaderComponent} from './layouts/header/header.component';
import {FooterComponent} from './layouts/footer/footer.component';

// service
import {UserRouteAccessService} from './shared/auth/user-route-access-service';

import {GrowlModule} from 'primeng/components/growl/growl';
import {PickListModule} from 'primeng/components/picklist/picklist';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxWebstorageModule} from 'ngx-webstorage';

// sections
import {HomeComponent} from './sections/home/home.component';
import {BlogComponent} from './sections/blog/blog.component';
import {PostListComponent} from './sections/blog/posts/post-list.component';
import {PodcastComponent} from './sections/podcast/podcast.component';

@NgModule({
    imports: [
        GrowlModule,
        BrowserModule,
        BrowserAnimationsModule,
        NgxWebstorageModule.forRoot({prefix: 'xmt', separator: '-'}),
        LayoutRoutingModule,
        PickListModule,
        HttpClientModule,
        NgbModule.forRoot(),
    ],
    declarations: [
        MainComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        BlogComponent,
        PostListComponent,
        PodcastComponent
    ],
    providers: [
        {provide: Window, useValue: window},
        UserRouteAccessService,
        PostListComponent
    ],
    bootstrap: [MainComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
