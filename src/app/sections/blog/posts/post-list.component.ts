import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Post} from './models/post';

@Component({
  selector: 'xmt-post-list-component',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[];

  constructor() {
  }

  ngOnInit() {
    this.posts = [
      {id:"1", author: "test Marad", publishDate: "10.04.1991", introduction:"some test text intro"},
      {id:"2", author: "test Marad", publishDate: "10.04.1991", introduction:"some test text intro"},
      {id:"3", author: "test Marad", publishDate: "10.04.1991", introduction:"some test text intro"},
      {id:"4", author: "Budnikov Artem", publishDate: "10.04.1991", introduction:"My happy birthday"},
    ];
  }

  ngOnDestroy() {

  }
}
