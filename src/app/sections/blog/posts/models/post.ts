export class Post {
  id: string;
  author: string;
  publishDate: string;
  introduction: string;
}
