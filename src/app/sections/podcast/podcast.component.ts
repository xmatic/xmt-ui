import {Component} from '@angular/core';

@Component({
  selector: 'xmt-podcast',
  templateUrl: './podcast.component.html',
  styleUrls: ['./podcast.component.css']
})

export class PodcastComponent {}
