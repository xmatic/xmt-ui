import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class UserRouteAccessService implements CanActivate {

  constructor() {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean | Promise<boolean> {
    return true;
  }
}
