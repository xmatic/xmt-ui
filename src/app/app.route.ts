import {HeaderComponent} from './layouts/header/header.component';
import {FooterComponent} from './layouts/footer/footer.component';
import {HomeComponent} from './sections/home/home.component';
import {BlogComponent} from './sections/blog/blog.component';
import {PostListComponent} from './sections/blog/posts/post-list.component';
import {PodcastComponent} from './sections/podcast/podcast.component';
import {UserRouteAccessService} from './shared/auth/user-route-access-service';

import {Routes} from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        component: HeaderComponent,
        outlet: 'header'
    },
    {
        path: '',
        component: FooterComponent,
        outlet: 'footer'
    },
    {
        path: '',
        component: HomeComponent,
        data: {
            pageTitle: 'xmatic'
        }
    },
    {
        path: 'blog',
        component: PostListComponent,
        canActivate: [UserRouteAccessService],
        data: {
            pageTitle: 'xmatic - Блог'
        },
        children: [
            {
                path: '',
                component: PostListComponent
            }
        ]
    },
    {
        path: 'podcast',
        component: PodcastComponent,
        data: {
            pageTitle: 'xmatic - Подкаст'
        }
    }
];
